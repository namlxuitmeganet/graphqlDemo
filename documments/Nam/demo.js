const koa = require('koa');
const koaRouter = require('koa-router');
const koaBody = require('koa-bodyparser');
const { graphqlKoa } = require('apollo-server-koa');
const { graphiqlKoa } = require('apollo-server-koa');
const app = new koa();


app.use('/graphiql', graphiqlKoa({
    endpointURL: '/graphql'
}));

app.use('/graphql', koaBody.json(), graphqlKoa({}));;

app.listen(4000, () => console.log(`Express server running on port 4000`));