const graphql = require('graphql');
const mongoose = require('mongoose');

const Post = require('../models/post');
const User = require('../models/user');

const { GraphQLObjectType, GraphQLString, GraphQLBoolean, GraphQLSchema, GraphQLID, GraphQLList, GraphQLNonNull } = graphql;

// Post schema
const PostType = new GraphQLObjectType({
    name: 'Post',
    fields: () => ({
        _id: { type: GraphQLID },
        title: { type: GraphQLString },
        description: { type: GraphQLString },
        content: { type: GraphQLString },
        updatedTime: { type: GraphQLString },
        user: {
            type: UserType,
            resolve(parent) {
                return new Promise((resolve, reject) => {
                    // get User by postId
                    User.findById(parent.user, (err, res) => {
                        err ? reject(err) : resolve(res);
                    });
                })
            }
        }
    })
});

// User schema
const UserType = new GraphQLObjectType({
    name: 'User',
    fields: () => ({
        _id: { type: GraphQLID },
        name: { type: GraphQLString },
        account: { type: GraphQLString },
        updatedTime: { type: GraphQLString },
        posts: {
            type: new GraphQLList(PostType),
            resolve(parent, args) {
                return new Promise((resolve, reject) => {
                    //get Post by userId
                    Post.find({ user: parent._id }, (err, res) => {
                        err ? reject(err) : resolve(res);
                    });
                })
            }
        }
    })
});

// Data respone schema
const MutationResType = new GraphQLObjectType({
    name: 'MutationRes',
    fields: () => ({
        success: { type: GraphQLBoolean },
        message: { type: GraphQLString }
    })
})

// Data respone with token schema
const MutationResTokenType = new GraphQLObjectType({
    name: 'MutationResToken',
    fields: () => ({
        success: { type: GraphQLBoolean },
        message: { type: GraphQLString },
        token: { type: GraphQLString }
    })
})

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        post: {
            type: PostType,
            args: { _id: { type: GraphQLID } },
            resolve(parent, args) {
                return new Promise((resolve, reject) => {
                    // get Post by postId
                    Post.findById(args._id, (err, res) => {
                        err ? reject(err) : resolve(res);
                    });
                })
            }
        },
        user: {
            type: UserType,
            args: { _id: { type: GraphQLID } },
            resolve(parent, args) {
                return new Promise((resolve, reject) => {
                    // get User by userId
                    User.findById(args._id, (err, res) => {
                        err ? reject(err) : resolve(res);
                    });
                })
            }
        },
        posts: {
            type: new GraphQLList(PostType),
            resolve(parent, args) {
                return new Promise((resolve, reject) => {
                    // get all Posts
                    Post.find({}, (err, res) => {
                        err ? reject(err) : resolve(res);
                    });
                })
            }
        },
        users: {
            type: new GraphQLList(UserType),
            resolve(parent, args) {
                return new Promise((resolve, reject) => {
                    // get all Users
                    User.find({}, (err, res) => {
                        err ? reject(err) : resolve(res);
                    });
                })
            }
        }
    }
});

const RootMutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        register: {
            type: MutationResTokenType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
                account: { type: new GraphQLNonNull(GraphQLString) },
                password: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parent, args) {
                return new Promise((resolve, reject) => {
                    // find User by account
                    User.findOne({ account: (args.account).toUpperCase() }, (err, res) => {
                        if (err) {
                            reject(err);
                        }
                        if (res) {
                            resolve({
                                success: false,
                                message: "Account exists!",
                                token: ""
                            })
                        } else {
                            let user = new User({
                                _id: new mongoose.Types.ObjectId(),
                                name: args.name,
                                account: (args.account).toUpperCase(),
                                password: bcrypt.hashSync(args.password, 8)
                            });

                            // add User
                            user.save((err, res) => {
                                if (err) {
                                    reject(err);
                                }

                                // generate a token
                                const token = jwt.sign({
                                    _id: res._id,
                                    name: res.name,
                                    account: res.account
                                }, 'secret', {
                                        expiresIn: 86400 // expires in 24 hours
                                    });

                                resolve({
                                    success: true,
                                    message: 'Registered Successfully!',
                                    token: token
                                });
                            })
                        }
                    })
                })
            }
        },
        logIn: {
            type: MutationResTokenType,
            args: {
                account: { type: new GraphQLNonNull(GraphQLString) },
                password: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parent, args) {
                return new Promise((resolve, reject) => {
                    // find User by account
                    User.findOne({ account: (args.account).toUpperCase() }, (err, res) => {
                        if (err) {
                            reject(err);
                        }
                        if (res) {
                            // compare userPassword
                            if (!bcrypt.compareSync(args.password, res.password)) {
                                resolve({
                                    success: false,
                                    message: "Password is incorrect!",
                                    token: ""
                                })
                            } else {
                                // generate a token
                                const token = jwt.sign({
                                    _id: res._id,
                                    name: res.name,
                                    account: res.account
                                }, 'secret', {
                                        expiresIn: 86400 // expires in 24 hours
                                    });

                                resolve({
                                    success: true,
                                    message: "Logged in successfully!",
                                    token: token
                                })
                            }
                        } else {
                            resolve({
                                success: false,
                                message: "Account does not exist!",
                                token: ""
                            })
                        }
                    })
                })
            }
        },
        addPost: {
            type: MutationResType,
            args: {
                title: { type: new GraphQLNonNull(GraphQLString) },
                description: { type: new GraphQLNonNull(GraphQLString) },
                content: { type: new GraphQLNonNull(GraphQLString) },
                user: { type: new GraphQLNonNull(GraphQLID) }
            },
            resolve(parent, args) {
                return new Promise((resolve, reject) => {
                    // get User by userId
                    User.findById(args.user, (err, res) => {
                        if (err) {
                            reject(err);
                        }
                        if (res) {
                            let post = new Post({
                                _id: new mongoose.Types.ObjectId(),
                                title: args.title,
                                description: args.description,
                                content: args.content,
                                user: args.user
                            })
        
                            // add Post
                            post.save((err, res) => {
                                err ? reject(err) : resolve({
                                    success: true,
                                    message: 'Posted successfully!'
                                });
                            });
                        } else {
                            resolve({
                                success: false,
                                message: 'The userId is incorrect!'
                            })
                        }
                    }) 
                })
            }
        },
        editPost: {
            type: MutationResType,
            args: {
                _id: { type: new GraphQLNonNull(GraphQLID) },
                title: { type: new GraphQLNonNull(GraphQLString) },
                description: { type: new GraphQLNonNull(GraphQLString) },
                content: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parent, args) {
                return new Promise((resolve, reject) => {
                    // get Post by postId then update
                    Post.findByIdAndUpdate(args._id, { $set: { title: args.title, description: args.description, content: args.content, updatedTime: new Date } }, (err, res) => {
                        if (err) {
                            reject(err);
                        }
                        if (res) {
                            resolve({
                                success: true,
                                message: 'Edited post successfully!'
                            });
                        } else {
                            resolve({
                                success: false,
                                message: 'The postId is incorrect!'
                            });
                        }
                    });
                })
            }
        },
        deletePost: {
            type: MutationResType,
            args: {
                _id: { type: new GraphQLNonNull(GraphQLID) }
            },
            resolve(parent, args) {
                return new Promise((resolve, reject) => {
                    // get Post by postId then remove
                    Post.findByIdAndRemove(args._id, (err, res) => {
                        if (err) {
                            reject(err);
                        }
                        if (res) {
                            resolve({
                                success: true,
                                message: 'Deleted post successfully!'
                            })
                        } else {
                            resolve({
                                success: false,
                                message: 'The postId does not exist!'
                            })
                        }
                    });
                })
            }
        }
    }
});

// export module
module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: RootMutation
});