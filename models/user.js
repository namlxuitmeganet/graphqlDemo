const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    _id: Schema.Types.ObjectId,
    name: {
        type: String,
        require: true
    },
    account: {
        type: String,
        require: true,
        unique: true
    },
    password: {
        type: String,
        require: true,
    },
    updatedTime: {
        type: Date,
        default: Date.now,
        require: true
    }
});

module.exports = mongoose.model('User', userSchema);