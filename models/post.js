const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const postSchema = new Schema({
    _id: Schema.Types.ObjectId,
    title: {
        type: String,
        require: true
    },
    content: {
        type: String,
        require: true
    },
    description: {
        type: String,
        require: true
    },
    user: {
        type: Schema.Types.ObjectId,
        require: true
    },
    updatedTime: {
        type: Date,
        default: Date.now,
        require: true
    }
});

module.exports = mongoose.model('Post', postSchema);